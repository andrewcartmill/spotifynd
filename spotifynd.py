#!/usr/bin/python

import optparse

import requests

if __name__ == "__main__":
    parser = optparse.OptionParser("usage: %prog [options] arg1 arg2")
    parser.add_option("-q", "--query", dest="query",
                      default="", type="string",
                      help="Search query. For multiple terms, use quotes. default: """)
    parser.add_option("-t", "--type", dest="queryType", default="artist,album,track,playlist",
                      type="string", help="Query type. default: artist, album, track, playlist")

    (options, args) = parser.parse_args()

    query = options.query

    queryType = options.queryType

    url = "https://api.spotify.com/v1/search?q=" + query + "&type=" + queryType + "&offset=0&limit=50"
    print("GET " + url)

    response = requests.get(url)
    data = response.json()

    if "artist" in queryType:

        print(" ")
        print("--------------------")
        print("Artists")
        print("--------------------")

        try:
            for id in data['artists']['items']:
                artistName = id['name']
                artistName = artistName.replace(u"\u2026", "...")
                artistName = (artistName[:50] + '...') if len(artistName) > 50 else artistName
                artistUri = id['external_urls']['spotify']
                artistUri = artistUri.replace(u"https://open.spotify.com/", "spotify://")
                print(artistName.ljust(60), artistUri.rjust(10))
        except:
            print("")

    if "album" in queryType:

        print(" ")
        print("--------------------")
        print("Albums")
        print("--------------------")

        try:
            for id in data['albums']['items']:
                albumsAlbumName = id['name']
                albumsAlbumName = albumsAlbumName.replace(u"\u2026", "...")
                albumsAlbumName = (albumsAlbumName[:50] + '...') if len(albumsAlbumName) > 50 else albumsAlbumName
                albumsUri = id['external_urls']['spotify']
                albumsUri = albumsUri.replace(u"https://open.spotify.com/", "spotify://")
                print(albumsAlbumName.ljust(60), albumsUri.ljust(60))
        except:
            print("")

    if "track" in queryType:

        print(" ")
        print("--------------------")
        print("Tracks")
        print("--------------------")

        try:
            for id in data['tracks']['items']:
                tracksName = id['name']
                tracksName = tracksName.replace(u"\u2026", "...")
                tracksName = (tracksName[:50] + '...') if len(tracksName) > 50 else tracksName
                tracksAlbumName = id['album']['name']
                tracksAlbumName = (tracksAlbumName[:50] + '...') if len(tracksAlbumName) > 50 else tracksAlbumName
                tracksAlbumName = tracksAlbumName.replace(u"\u2026", "...")
                tracksUri = id['external_urls']['spotify']
                tracksUri = tracksUri.replace(u"https://open.spotify.com/", "spotify://")
                print(tracksName.ljust(60), tracksAlbumName.ljust(60), tracksUri.rjust(10))
        except:
            print("")

    if "playlist" in queryType:

        print(" ")
        print("--------------------")
        print("Playlists")
        print("--------------------")

        try:
            for id in data['playlists']['items']:
                playlistName = id['name']
                playlistName = playlistName.replace(u"\u2026", "...")
                playlistName = (playlistName[:50] + '...') if len(playlistName) > 50 else playlistName
                playlistUri = id['external_urls']['spotify']
                playlistUri = (playlistUri[:64] + '...') if len(playlistUri) > 64 else playlistUri
                playlistUri = playlistUri.replace(u"http://open.spotify.com/", "spotify://")
                print(playlistName.ljust(60), playlistUri.ljust(60))
        except:
            print("")
